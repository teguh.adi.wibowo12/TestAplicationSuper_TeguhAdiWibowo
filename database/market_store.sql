-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 26, 2023 at 02:45 PM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 7.2.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `market_store`
--

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `CustomerID` int(10) NOT NULL,
  `CustomerName` varchar(100) NOT NULL,
  `DateOfBirth` date NOT NULL,
  `Address` text NOT NULL,
  `Email` varchar(100) NOT NULL,
  `PhoneNo` varchar(15) NOT NULL,
  `IsActive` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`CustomerID`, `CustomerName`, `DateOfBirth`, `Address`, `Email`, `PhoneNo`, `IsActive`) VALUES
(1, 'cust1', '1992-04-12', 'Jakarta Barat', 'cust1@gmail.com', '081234567890', 1);

-- --------------------------------------------------------

--
-- Table structure for table `inventoryin`
--

CREATE TABLE `inventoryin` (
  `TransactionID` int(11) NOT NULL,
  `WarehouseID` int(10) NOT NULL,
  `ProductID` int(10) NOT NULL,
  `PriceBuy` decimal(15,0) NOT NULL,
  `QtyIn` int(11) NOT NULL,
  `IsProccessDone` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `inventoryin`
--

INSERT INTO `inventoryin` (`TransactionID`, `WarehouseID`, `ProductID`, `PriceBuy`, `QtyIn`, `IsProccessDone`) VALUES
(1, 1, 1, '25000', 40, 1),
(2, 1, 1, '25000', 20, 1),
(2, 1, 3, '200000', 5, 1);

-- --------------------------------------------------------

--
-- Table structure for table `inventoryout`
--

CREATE TABLE `inventoryout` (
  `TransactionID` int(11) NOT NULL,
  `WarehouseID` int(10) NOT NULL,
  `ProductID` int(10) NOT NULL,
  `PriceSell` decimal(15,0) NOT NULL,
  `QtyOut` int(11) NOT NULL,
  `IsProccessDone` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `inventoryout`
--

INSERT INTO `inventoryout` (`TransactionID`, `WarehouseID`, `ProductID`, `PriceSell`, `QtyOut`, `IsProccessDone`) VALUES
(1, 1, 1, '25000', 4, 1);

-- --------------------------------------------------------

--
-- Table structure for table `itemsorder`
--

CREATE TABLE `itemsorder` (
  `TransactionID` int(10) NOT NULL,
  `ProductID` int(10) NOT NULL,
  `Qty` bigint(10) NOT NULL,
  `Price` decimal(15,0) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `itemsorder`
--

INSERT INTO `itemsorder` (`TransactionID`, `ProductID`, `Qty`, `Price`) VALUES
(1, 1, 40, '25000'),
(2, 1, 20, '25000'),
(2, 3, 200000, '5');

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `ProductID` int(10) NOT NULL,
  `ProductName` varchar(100) NOT NULL,
  `ProductDesc` text NOT NULL,
  `Unit` varchar(30) NOT NULL,
  `IsActive` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`ProductID`, `ProductName`, `ProductDesc`, `Unit`, `IsActive`) VALUES
(1, 'Sabun', 'Sabun Cair', 'pcs', 1),
(2, 'Sabun Batang', 'Sabun Batang', 'pcs', 1),
(3, 'Sabun Cair', 'Sabun Cair', 'box', 1);

-- --------------------------------------------------------

--
-- Table structure for table `purchaseorder`
--

CREATE TABLE `purchaseorder` (
  `TransactionID` int(10) NOT NULL,
  `TransactionDate` datetime NOT NULL,
  `SupplierID` int(10) NOT NULL,
  `TotalPrice` decimal(15,0) NOT NULL,
  `TotalDisc` decimal(15,0) NOT NULL,
  `TotalPayment` decimal(15,0) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `purchaseorder`
--

INSERT INTO `purchaseorder` (`TransactionID`, `TransactionDate`, `SupplierID`, `TotalPrice`, `TotalDisc`, `TotalPayment`) VALUES
(1, '2023-03-26 13:14:00', 1, '1000000', '0', '1000000'),
(2, '2023-03-26 16:00:00', 2, '1500000', '0', '1500000');

-- --------------------------------------------------------

--
-- Table structure for table `recieveorder`
--

CREATE TABLE `recieveorder` (
  `TransactionID` int(10) NOT NULL,
  `TransactionDate` datetime NOT NULL,
  `POID` int(10) NOT NULL,
  `ReceiveName` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `recieveorder`
--

INSERT INTO `recieveorder` (`TransactionID`, `TransactionDate`, `POID`, `ReceiveName`) VALUES
(1, '2023-03-26 15:00:00', 1, 'PIC1'),
(2, '2023-03-26 18:00:00', 2, 'PO2');

-- --------------------------------------------------------

--
-- Table structure for table `salesorder`
--

CREATE TABLE `salesorder` (
  `TransactionID` int(10) NOT NULL,
  `TransactionDate` datetime NOT NULL,
  `CustomerID` int(10) NOT NULL,
  `TotalPrice` decimal(15,0) NOT NULL,
  `TotalDisc` decimal(15,0) NOT NULL,
  `TotalPayment` decimal(15,0) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `salesorder`
--

INSERT INTO `salesorder` (`TransactionID`, `TransactionDate`, `CustomerID`, `TotalPrice`, `TotalDisc`, `TotalPayment`) VALUES
(1, '2023-03-26 18:00:00', 1, '100000', '0', '100000');

-- --------------------------------------------------------

--
-- Table structure for table `supplier`
--

CREATE TABLE `supplier` (
  `SupplierID` int(10) NOT NULL,
  `SupplierName` varchar(100) NOT NULL,
  `Address` text NOT NULL,
  `PIC` varchar(100) NOT NULL,
  `PhoneNo` varchar(15) NOT NULL,
  `Email` varchar(100) NOT NULL,
  `IsActive` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `supplier`
--

INSERT INTO `supplier` (`SupplierID`, `SupplierName`, `Address`, `PIC`, `PhoneNo`, `Email`, `IsActive`) VALUES
(1, 'Supp1', 'supp1', 'Supp', '081234567890', 'supp1@gmail.com', 1),
(2, 'supp2', 'supp2', 'supp2', '081236547890', 'supp2@gmail.com', 1);

-- --------------------------------------------------------

--
-- Table structure for table `warehouse`
--

CREATE TABLE `warehouse` (
  `WarehouseID` int(10) NOT NULL,
  `WarehouseName` varchar(100) NOT NULL,
  `Address` text NOT NULL,
  `PhoneNo` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `warehouse`
--

INSERT INTO `warehouse` (`WarehouseID`, `WarehouseName`, `Address`, `PhoneNo`) VALUES
(1, 'Alam Sutra', 'Alam Sutra, Tangerang', '081234567890'),
(2, 'Surabaya', 'Surabaya', '081236547890');

-- --------------------------------------------------------

--
-- Table structure for table `warehousestock`
--

CREATE TABLE `warehousestock` (
  `WarehouseID` int(10) NOT NULL,
  `ProductID` int(10) NOT NULL,
  `PriceSell` decimal(15,0) NOT NULL,
  `MinStock` bigint(10) NOT NULL DEFAULT '0',
  `CurrStock` bigint(10) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `warehousestock`
--

INSERT INTO `warehousestock` (`WarehouseID`, `ProductID`, `PriceSell`, `MinStock`, `CurrStock`) VALUES
(1, 1, '0', 0, 56),
(1, 3, '0', 10, 5);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`CustomerID`);

--
-- Indexes for table `inventoryin`
--
ALTER TABLE `inventoryin`
  ADD PRIMARY KEY (`TransactionID`,`WarehouseID`,`ProductID`) USING BTREE;

--
-- Indexes for table `inventoryout`
--
ALTER TABLE `inventoryout`
  ADD PRIMARY KEY (`TransactionID`,`WarehouseID`,`ProductID`) USING BTREE;

--
-- Indexes for table `itemsorder`
--
ALTER TABLE `itemsorder`
  ADD PRIMARY KEY (`TransactionID`,`ProductID`) USING BTREE,
  ADD KEY `ProductOrder` (`ProductID`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`ProductID`);

--
-- Indexes for table `purchaseorder`
--
ALTER TABLE `purchaseorder`
  ADD PRIMARY KEY (`TransactionID`),
  ADD KEY `SupplierPO` (`SupplierID`);

--
-- Indexes for table `recieveorder`
--
ALTER TABLE `recieveorder`
  ADD PRIMARY KEY (`TransactionID`),
  ADD KEY `POReceive` (`POID`);

--
-- Indexes for table `salesorder`
--
ALTER TABLE `salesorder`
  ADD PRIMARY KEY (`TransactionID`),
  ADD KEY `CustomerOrder` (`CustomerID`);

--
-- Indexes for table `supplier`
--
ALTER TABLE `supplier`
  ADD PRIMARY KEY (`SupplierID`);

--
-- Indexes for table `warehouse`
--
ALTER TABLE `warehouse`
  ADD PRIMARY KEY (`WarehouseID`);

--
-- Indexes for table `warehousestock`
--
ALTER TABLE `warehousestock`
  ADD PRIMARY KEY (`WarehouseID`,`ProductID`),
  ADD KEY `ProductID` (`ProductID`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `inventoryin`
--
ALTER TABLE `inventoryin`
  ADD CONSTRAINT `ReceiveOrderID` FOREIGN KEY (`TransactionID`) REFERENCES `recieveorder` (`TransactionID`) ON UPDATE NO ACTION;

--
-- Constraints for table `inventoryout`
--
ALTER TABLE `inventoryout`
  ADD CONSTRAINT `TransactionSO` FOREIGN KEY (`TransactionID`) REFERENCES `salesorder` (`TransactionID`);

--
-- Constraints for table `itemsorder`
--
ALTER TABLE `itemsorder`
  ADD CONSTRAINT `ProductOrder` FOREIGN KEY (`ProductID`) REFERENCES `product` (`ProductID`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `TransactionPO` FOREIGN KEY (`TransactionID`) REFERENCES `purchaseorder` (`TransactionID`) ON UPDATE NO ACTION;

--
-- Constraints for table `purchaseorder`
--
ALTER TABLE `purchaseorder`
  ADD CONSTRAINT `SupplierPO` FOREIGN KEY (`SupplierID`) REFERENCES `supplier` (`SupplierID`) ON UPDATE NO ACTION;

--
-- Constraints for table `recieveorder`
--
ALTER TABLE `recieveorder`
  ADD CONSTRAINT `POReceive` FOREIGN KEY (`POID`) REFERENCES `purchaseorder` (`TransactionID`) ON UPDATE NO ACTION;

--
-- Constraints for table `salesorder`
--
ALTER TABLE `salesorder`
  ADD CONSTRAINT `CustomerOrder` FOREIGN KEY (`CustomerID`) REFERENCES `customer` (`CustomerID`) ON UPDATE NO ACTION;

--
-- Constraints for table `warehousestock`
--
ALTER TABLE `warehousestock`
  ADD CONSTRAINT `ProductID` FOREIGN KEY (`ProductID`) REFERENCES `product` (`ProductID`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `WarehouseID` FOREIGN KEY (`WarehouseID`) REFERENCES `warehouse` (`WarehouseID`) ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
