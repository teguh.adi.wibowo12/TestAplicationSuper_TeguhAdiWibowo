import express from 'express';
import controller from '../controllers/warehouse';

const router = express.Router();

router.get('/productstock', controller.getProductStock)
router.get('/productstock/:id', controller.getProductStockByID)

router.get('/productstockupdate', controller.updateAllProductStockLevel)
router.post('/productstock', controller.updateProductStockbyWHProduct)

router.get('/productstocklevelbelow', controller.getProductStockLevelBelow)


export = router;