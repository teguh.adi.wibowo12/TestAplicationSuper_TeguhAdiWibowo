import { Request, Response, NextFunction } from 'express';

import config from '../config/database';
import mysql from 'mysql';
import { CONNREFUSED } from 'dns';

const pool = mysql.createPool(config);

pool.on('error', (err) => {
    console.error(err);
})

const getProductStock= async (req: Request, res: Response, next: NextFunction) => {
    pool.getConnection(function(err, connection){
        if (err) throw err;
        connection.query(
            `SELECT t0.WarehouseID,  t0.ProductID, t1.ProductName,  t1.ProductDesc, t1.Unit,  t0.PriceSell,  t0.MinStock,  t0.CurrStock 
            FROM warehouse as t0
            LEFT JOIN product as t1 on t1.ProductID = t0.ProductID
            WHERE t1.IsActive = 1`,
            function (error, results){
                if (error) throw err;
                res.send({
                    success: true,
                    message: 'Get Data Succeed',
                    data: results
                });
            }
        );
        connection.release();
    })
}

// TEST NO 2
const getProductStockByID= async (req: Request, res: Response, next: NextFunction) => {
    let id = req.params.id;
    pool.getConnection(function(err, connection){
        if (err) throw err;
        connection.query(
            `SELECT t0.ProductID, t1.ProductName,  t1.ProductDesc, t1.Unit,  SUM(t0.CurrStock) CurrStock 
            FROM warehouse as t0
            LEFT JOIN product as t1 on t1.ProductID = t0.ProductID
            WHERE t1.IsActive = 1 AND t1.ProductID = ?
            GROUP BY t0.ProductID, t1.ProductName, t1.ProductDesc, t1.Unit`,
            [id],
            function (error, results){
                if (error) throw err;
                res.send({
                    success: true,
                    message: 'Get Data Succeed',
                    data: results
                });
            }
        );
        connection.release();
    })
}


// TEST NO 3
const updateAllProductStockLevel= async (req: Request, res: Response, next: NextFunction) => {
    pool.getConnection(function(err, connection){
        if (err) throw err;
        connection.query(
            `CREATE TEMPORARY TABLE IF NOT EXISTS tempInvIn(TransactionID INT, WarehouseID INT, ProductID INT, Price INT, MinStock INT, Qty INT);

            INSERT INTO tempInvIn
            SELECT t0.TransactionID, t0.WarehouseID, t0.ProductID, 0 Price, 0 MinStock, t0.QtyIn
            FROM inventoryin as t0
            LEFT JOIN warehousestock as t1 on t1.WarehouseID = t0.WarehouseID and t1.ProductID = t0.ProductID
            WHERE t0.IsProccessDone = 0
            AND ISNULL(t1.ProductID) = 1
            ;
            
            INSERT into warehousestock
            select WarehouseID, ProductID, Price, MinStock, Qty from tempInvIn
            ;
            
            UPDATE inventoryin t0
            INNER JOIN tempInvIn t1 on t1.TransactionID = t0.TransactionID and t1.WarehouseID = t0.WarehouseID and t1.ProductID = t0.ProductID
            SET t0.IsProccessDone = 1
            ;
            
            
            
            CREATE TEMPORARY TABLE IF NOT EXISTS tempInvUpdate(TransactionID INT, WarehouseID INT, ProductID INT, Qty INT, Status Varchar(10));
            
            INSERT INTO tempInvUpdate
            SELECT t3.transactionID, t3.WarehouseID, t3.ProductID, t3.Qty, t3.Status
            FROM (
                SELECT t1.TransactionID, t1.WarehouseID, t1.ProductID, t1.QtyIn as Qty, 'IN' as Status
                FROM inventoryin as t1
                WHERE t1.IsProccessDone = 0
                UNION ALL
                SELECT t2.TransactionID, t2.WarehouseID, t2.ProductID, (t2.QtyOut*-1) Qty, 'OUT' as Status
                FROM inventoryout as t2
                WHERE t2.IsProccessDone = 0
            ) t3
            ;
                
            UPDATE warehousestock as t0
            INNER JOIN (
                SELECT t3.WarehouseID, t3.ProductID, sum(t3.Qty) as Qty
                FROM tempInvUpdate t3
                GROUP BY t3.WarehouseID, t3.ProductID
            ) as t1 on t1.WarehouseID = t0.WarehouseID and t1.ProductID = t0.ProductID
            SET t0.CurrStock = t0.CurrStock + t1.Qty
            ;
            
            UPDATE inventoryin t0
            INNER JOIN tempInvUpdate t1 on t1.TransactionID = t0.TransactionID and t1.WarehouseID = t0.WarehouseID and t1.ProductID = t0.ProductID and t1.Status = 'IN'
            SET t0.IsProccessDone = 1
            ;
            
            UPDATE inventoryout t0
            INNER JOIN tempInvUpdate t1 on t1.TransactionID = t0.TransactionID and t1.WarehouseID = t0.WarehouseID and t1.ProductID = t0.ProductID and t1.Status = 'OUT'
            SET t0.IsProccessDone = 1
            ;
            
            drop TEMPORARY table tempInvIn;
            drop TEMPORARY table tempInvUpdate;
            
            select true as result, 'Data has been update' as message;
            `,
            function (error, results){
                if (error) throw err;
                res.send({
                    success: true,
                    message: 'Update Data Succeed',
                    data: results
                });
            }
        );
        connection.release();
    })
}

const updateProductStockbyWHProduct= async (req: Request, res: Response, next: NextFunction) => {
    let warehouseid = req.body.warehouseid;
    let productid = req.body.productid;
    let qty = req.body.qty;
    if (req.body.StockStats == 'OUT'){ qty = qty * -1;}
    pool.getConnection(function(err, connection){
        if (err) throw err;
        connection.query(
            `UPDATE warehousestock as t0
            SET t0.CurrStock = t0.CurrStock + ?
            WHERE t0.WarehouseID = ? and t0.ProductID = ?
            ;`,
            [qty, warehouseid, productid],
            function (error, results){
                if (error) throw err;
                res.send({
                    success: true,
                    message: 'Update Data Succeed',
                    data: results
                });
            }
        );
        connection.release();
    })
}

// TEST NO 4
const getProductStockLevelBelow= async (req: Request, res: Response, next: NextFunction) => {
    pool.getConnection(function(err, connection){
        if (err) throw err;
        connection.query(
            `SELECT T1.WarehouseName, t1.Address, T2.ProductName, T2.ProductDesc, T2.Unit, t0.MinStock, t0.CurrStock FROM warehousestock t0
            LEFT JOIN warehouse t1 on t1.WarehouseID = t0.WarehouseID
            LEFT JOIN product t2 on t2.ProductID = t0.ProductID
            WHERE t0.MinStock >= t0.CurrStock and t2.IsActive = 1`,
            function (error, results){
                if (error) throw err;
                res.send({
                    success: true,
                    message: 'Get Data Succeed',
                    data: results
                });
            }
        );
        connection.release();
    })
}



export default { getProductStock, getProductStockByID, updateAllProductStockLevel, updateProductStockbyWHProduct, getProductStockLevelBelow };